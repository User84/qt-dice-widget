#ifndef DICEWIDGET_H
#define DICEWIDGET_H

#include "dice.h"

#include <QWidget>
#include <QPixmap>
#include <QGraphicsScene>

#define DEFAULT_STOP_TIME 2500


namespace Ui {
class DiceWidget;
}

class DiceWidget : public QWidget
{
    Q_OBJECT

public:
    explicit DiceWidget(Dice::Color color, QWidget *parent = nullptr);
    explicit DiceWidget(Dice::Color color, bool autoRoll, QWidget *parent = nullptr);

    ~DiceWidget();


    int stopTime() const;
    void setStopTime(int stopTime);

private:
    Ui::DiceWidget *ui;
    QGraphicsScene *_scene;
    Dice *_d1,*_d2;
    Dice::Color _color;
    int _stopTime;

public slots:

    void exit();
    void stopSpinning();

signals:

    void done(int value);

private slots:
    void on_pushButton_clicked();
};

#endif // DICEWIDGET_H
