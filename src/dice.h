#ifndef DICE_H
#define DICE_H

#include <QGraphicsItem>
#include <QTimer>

#define DIE_SIZE 6
#define SWITCH_TIME 20



class Dice : public QGraphicsObject
{
public:
    enum Color{
        Red,
        Yellow,
        Green,
        Blue,
        Purple,
        White
    };
private:
    Q_OBJECT
    QPixmap *_spriteSheet;
    QTimer *_timer;
    Color _color;
    int _index;
public:
    Dice(Color color);
    ~Dice();

    QRectF boundingRect() const;

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

    void cycle();
    int index() const;

    void stop();
    void start();
};

#endif // DICE_H
