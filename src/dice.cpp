#include "dice.h"

#include <QPainter>

Dice::Dice(Color color)
{
    _color = color;
    _index = qrand() % DIE_SIZE;
    _spriteSheet = new QPixmap(":/dice_images/dice.jpg");

    _timer = new QTimer(this);
    connect(_timer, &QTimer::timeout, this, &Dice::cycle);
}

Dice::~Dice()
{
    delete _spriteSheet;
}

QRectF Dice::boundingRect() const
{
    return QRectF(0,0,63,63);
}

void Dice::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);
    QRectF source(_index * boundingRect().width(),_color * boundingRect().width(),boundingRect().width(), boundingRect().height());
    QPixmap pm = _spriteSheet->copy(source.toRect());
    QBrush brush(pm);
    painter->setBrush(brush);
    painter->drawRoundedRect(boundingRect(), 5,5);
}

int Dice::index() const
{
    return _index;
}

void Dice::stop()
{
    _timer->stop();
}

void Dice::start()
{
    _timer->start(SWITCH_TIME);
}

void Dice::cycle()
{
    _timer->setInterval(_timer->interval()+5);
    _index = qrand() % DIE_SIZE;
    update();
}
