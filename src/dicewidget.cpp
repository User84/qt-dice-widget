#include "dicewidget.h"
#include "ui_dicewidget.h"
#include "dice.h"

DiceWidget::DiceWidget(Dice::Color color, bool autoRoll, QWidget *parent):
    QWidget(parent),
    ui(new Ui::DiceWidget)
{
    ui->setupUi(this);

    setWindowTitle("Roll Dice");

    _color = color;
    _stopTime = DEFAULT_STOP_TIME;

    _scene = new QGraphicsScene(this);
    ui->graphicsView->setScene(_scene);

    _d1 = new Dice(_color);
    _scene->addItem(_d1);

    _d2 = new Dice(_color);
    _d2->moveBy(_d1->boundingRect().width() + 10, 0);
    _scene->addItem(_d2);

    QBrush brush(Qt::black);
    _scene->setBackgroundBrush(brush);

    if(autoRoll)
    {
        on_pushButton_clicked();
    }
}

DiceWidget::~DiceWidget()
{
    delete _d1;
    delete _d2;
    delete ui;
}

int DiceWidget::stopTime() const
{
    return _stopTime;
}

void DiceWidget::setStopTime(int stopTime)
{
    _stopTime = stopTime;
}

void DiceWidget::exit()
{
    int total = _d1->index() + 1 + _d2->index() + 1;
    emit done(total);
    deleteLater();
}

void DiceWidget::stopSpinning()
{
    _d1->stop();
    _d2->stop();
    QTimer::singleShot(stopTime(),this, &DiceWidget::exit);
}

void DiceWidget::on_pushButton_clicked()
{
    ui->pushButton->setEnabled(false);
    _d1->start();
    _d2->start();
    _d1->cycle();
    _d2->cycle();
    QTimer::singleShot(2000,this, &DiceWidget::stopSpinning);

}
