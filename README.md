# Qt-Dice-Widget

A Qt designer form class for rolling dice.

![screenshot](/images/screenshot.png)

```c++
#include "qt-dice-widget/src/dicewidget.h"

```
```c++
DiceWidget *dice = new DiceWidget(DiceColor::Red);
connect(dice, &DiceWidget::done, this, &MainWindow::setValue);
dice.show();

```





